#!/bin/bash

# Dapatkan versi dari tag
VERSION=$CI_COMMIT_TAG

# Tentukan file environment yang akan diupdate
ENV_FILES=(".env.dev" ".env.production")

for FILE in "${ENV_FILES[@]}"; do
  if [ -f "$FILE" ]; then
    echo "Updating version in $FILE to $VERSION"
    sed -i "s/VUE_APP_VERSION=.*/VUE_APP_VERSION=$VERSION/" $FILE
  else
    echo "$FILE does not exist."
  fi
done
